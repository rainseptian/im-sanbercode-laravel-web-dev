<?php
    require_once('animal.php');
    require('frog.php');
    require('ape.php');


    $sheep = new Animal("shaun");

    echo "Name  : " . $sheep->name . "<br>";
    echo "legs  : " . $sheep->legs . "<br>";
    echo "cold_blooded: " . $sheep->cold_blooded . "<br><br>";

    $kodok = new frog("buduk");

    echo "Name  : " . $kodok->name . "<br>";
    echo "legs  : " . $kodok->legs . "<br>";
    echo "cold_blooded: " . $kodok->cold_blooded . "<br>";
    echo "Jump  : " . $kodok->jump() . "<br><br>";

    $sungokong = new ape("kera sakti");

    echo "Name  : " . $sungokong->name . "<br>";
    echo "legs  : " . $sungokong->legs . "<br>";
    echo "cold_blooded: " . $sungokong->cold_blooded . "<br>";
    echo "Yell  : " . $sungokong->yell() . "<br>";    
?>
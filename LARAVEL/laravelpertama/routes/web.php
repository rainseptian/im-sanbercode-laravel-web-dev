<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Dashboardcontroller;
use App\Http\Controllers\Daftarcontroller;
use App\Http\Controllers\GetController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [Dashboardcontroller::class,'utama']);
Route::get('/daftar', [Daftarcontroller::class,'register']);
Route::post('/home', [Daftarcontroller::class,'home']);

Route::get('/data-table', function(){
    return view('page.data-table');
});
Route::get('/table', function(){
    return view('page.table');
});

//CRUD KATEGORI
//CREATE
//Form Tambah Kategori
Route::Get('/categories/create',[ GetController::class, 'create']);
//tambah data ke database
Route::post('/categories', [GetController::class, 'add']);

//READ
Route::Get('/categories',[GetController::class, 'read']);
Route::Get('/categories/{id}',[GetController::class, 'show']);

//UPDATE
Route::Get('/categories/{id}/edit',[GetController::class, 'edit']);
Route::put('/categories/{id}',[GetController::class, 'update']);

//DELETE
Route::delete('/categories/{id}',[GetController::class, 'destroy']);

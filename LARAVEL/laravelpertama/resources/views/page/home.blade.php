@extends('layout.master')
@section('title')
    Home
@endsection
@section('subtitle')
    Pendaftaran
@endsection
@section('content')
    <h1>Selamat Datang {{$namaDepan}} {{$namaBelakang}}</h1>
    <br>
    <br>
    <h2>Terimakasih telah bergabung di Sanberbook! Social Media kita bersama.</h2>
@endsection

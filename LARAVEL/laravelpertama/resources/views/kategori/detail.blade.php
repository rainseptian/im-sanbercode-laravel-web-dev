@extends('layout.master')
@section('title')
    Halaman menampilkan detail
@endsection
@section('subtitle')
    semua detail Kategori
@endsection
@section('content')
    <h1>{{$categories->name}}</h1>
    <p>{{$categories->description}}</p>

    <a href="/categories" class="btn btn-secondary btn-sm">Kembali</a>

@endsection

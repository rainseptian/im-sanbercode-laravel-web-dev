@extends('layout.master')
@section('title')
    Halaman Edit
@endsection
@section('subtitle')
    Kategori Edit
@endsection
@section('content')
<form action="/categories/{{$categories->id}}" method="POST">
    @method('put')
    @csrf
    <div class="form-group">
        <label>Nama Kategori</label>
        <input type="text" name="nama" class="form-control" value="{{$categories->name}}">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Deskripsi</label>
        <textarea name="description" class="form-control" cols="30" rows="10">{{$categories->description}}</textarea>
    </div>
    @error('description')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection

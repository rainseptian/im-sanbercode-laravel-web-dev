@extends('layout.master')
@section('title')
    Halaman menampilkan Category
@endsection
@section('subtitle')
    semua Kategori
@endsection
@section('content')

<a href="/categories/create" class="btn btn-primary my-3">Tambah</a>

<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">Nomer</th>
        <th scope="col">Name</th>
        <th scope="col">Action</th>
    </tr>
    </thead>
    <tbody>
    @forelse ($categories as $key => $val)
    <tr>
        <th scope="row">{{$key+1}}</th>
        <td>{{$val->name}}</td>
        <td>
            <form action="/categories/{{$val->id}}" method="POST">
            <a href="/categories/{{$val->id}}" class="btn btn-info btn-sm">Detail</a>
            <a href="/categories/{{$val->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
            @csrf
            @method('delete')
            <input type="submit" class="btn btn-danger btn-sm" value="Delete">
        </form>
    </tr>
    @empty
        <p>Data tidak ditemukan</p>
    @endforelse
    </tbody>
</table>
@endsection

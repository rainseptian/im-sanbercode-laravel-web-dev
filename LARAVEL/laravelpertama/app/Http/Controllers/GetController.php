<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GetController extends Controller
{
    public function create()
    {
        return view('kategori.tambah');
    }
    public function add(Request $request)
    {
        $validated = $request->validate([
            'nama' => 'required',
            'description' => 'required',
        ]);
        DB::table('categories')->insert(
            [
                'name' => $request['nama'],
                'description' => $request['description']]
        );
        return redirect('/categories');
    }
    public function read()
    {
        $categories = DB::table('categories')->get();

        return view('kategori.read', ['categories'=>$categories]);
    }
    public function show($id)
    {
        $categories = DB::table('categories')->find($id);
        return view('kategori.detail', ['categories'=>$categories]);
    }
    public function edit($id)
    {
        $categories= DB::table('categories')->find($id);
        return view('kategori.edit',['categories'=>$categories]);
    }
    public function update($id, Request $request)
    {
        $validated = $request->validate([
            'nama' => 'required',
            'description' => 'required',
        ]);
            DB::table('categories')
                ->where('id', $id)
                ->update(
                    [
                        'name' => $request->input('nama'),
                        'description'=>$request->input('description')]);
        return redirect('/categories');
    }
    public function destroy($id)
    {
        DB::table('categories') ->where('id',$id ) ->delete();
        return redirect('/categories');
    }
}
